package controllers

import (
	"net/http"

	"sieko_core/models"

	"github.com/gin-gonic/gin"

	_ "sieko_core/app/docs"
)

// @Summary use Anonymous field
// @Success 200 {object} controllers.Auth "ok"
func Auth(c *gin.Context) {
	userReq := models.AuthSchema{}

	c.BindJSON(&userReq)
	if user := models.GetUserByUsername(userReq.Username); user.ID != 0 {
		err := user.CheckPassword(userReq.Password)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusForbidden, gin.H{
				"data": nil,
				"msg":  "Invalid credentials",
			})
			return
		}
		token, _ := user.GenerateToken()

		c.AbortWithStatusJSON(http.StatusOK, gin.H{
			"data": gin.H{
				"auth_token": token,
			},
			"msg": "User authenticated!!",
		})
		return
	}
	c.AbortWithStatusJSON(http.StatusForbidden, gin.H{
		"data": nil,
		"msg":  "Username and password incorrect",
	})
}
