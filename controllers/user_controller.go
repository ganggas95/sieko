package controllers

import (
	"net/http"
	"sieko_core/models"

	"github.com/gin-gonic/gin"
)

//NewUser Controller to create new User
func NewUser(c *gin.Context) {
	var userReq models.UserSchema
	c.BindJSON(&userReq)
	user := &models.User{}
	user.Username = userReq.Username
	err := user.CreatePassword(userReq.Password)
	if err != nil {
		panic(err)
	}
	user.IsStoreOwner = userReq.IsStoreOwner
	user.IsMember = userReq.IsMember
	user.CreateOne()
	c.JSON(http.StatusOK, gin.H{
		"data": user,
		"msg":  "Data success saved!",
	})
}

// GetUser Controller to handle request get user. params: user_id, Allowed method :['GET','POST']
func GetUser(c *gin.Context) {
	idUser := c.Param("user_id")
	user := models.GetUserById(idUser)
	if user.ID != 0 {
		switch c.Request.Method {
		case "GET":
			c.AbortWithStatusJSON(http.StatusOK, gin.H{
				"data": user,
				"msg":  "Data user received!!",
			})
			break
		case "POST":
			userReq := models.UserSchema{}
			c.BindJSON(&userReq)
			user.Username = userReq.Username
			user.Update()
			c.JSON(http.StatusCreated, gin.H{
				"data": user,
				"msg":  "Data saved",
			})
			break
		default:
			c.AbortWithStatusJSON(http.StatusMethodNotAllowed, gin.H{
				"data": nil,
				"msg":  "Method not allowed",
			})
			break
		}
	} else {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{
			"data": nil,
			"msg":  "Data not found",
		})
	}
}
