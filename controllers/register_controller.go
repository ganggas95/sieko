package controllers

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

//RegisterStoreController : Controller to handle Register Store
func RegisterStoreController(c *gin.Context){
	c.AbortWithStatusJSON(http.StatusOK, gin.H{
		"data": nil,
		"msg": "Register Store Successfully",
	})
}

//RegisterMemberController : Controller to handle Register New Member
func RegisterMemberController(c *gin.Context){
	c.AbortWithStatusJSON(http.StatusOK, gin.H{
		"data": nil,
		"msg": "Register Member Successfully",
	})
}

//ConfirmEmailController : Controller to handle Confirm Email Controller
func ConfirmEmailController(c *gin.Context){
	c.AbortWithStatusJSON(http.StatusOK, gin.H{
		"data": nil,
		"msg": "Confirm Email Successfully",
	})
}

//ConfirmFacebookController : Controller to handle Confirm Facebook Register
func ConfirmFacebookController(c *gin.Context){
	c.AbortWithStatusJSON(http.StatusOK, gin.H{
		"data": nil,
		"msg": "Confirm Facebook Successfully",
	})
}

//ConfirmPhoneNumberController : Controller to handle Confirm Number Registration
func ConfirmPhoneNumberController(c *gin.Context){
	c.AbortWithStatusJSON(http.StatusOK, gin.H{
		"data": nil,
		"msg": "Confirm PhoneNumber Successfully",
	})
}
