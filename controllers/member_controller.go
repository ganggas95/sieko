package controllers

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func MyProfile(c *gin.Context){
	c.JSON(http.StatusOK, gin.H{
		"view": "Member",
	})
}
