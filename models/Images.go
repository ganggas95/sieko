package models

import "github.com/jinzhu/gorm"

type Image struct {
	gorm.Model
	Path  string `gorm:"type:varchar(500)"`
	Size  float64
	Thumb string `gorm:"type:varchar(500)"`
}
