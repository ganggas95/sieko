package models

import (
	"sieko_core/common"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	Username     string `gorm:"type:varchar(45);unique;index:username"`
	Password     []byte `json:"-"`
	IsMember     bool
	IsStoreOwner bool
	IsCashier    bool
	gorm.Model
}

type UserStore struct {
	gorm.Model
	UserID  int64 `gorm:"type:bigint;index:user_id"`
	StoreID int64 `gorm:"type:bigint;index:store_id"`
}

type AuthSchema struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type UserSchema struct {
	Username     string `json:"username"`
	Password     string `json:"password"`
	IsMember     bool   `json:"is_member"`
	IsStoreOwner bool   `json:"is_store"`
}

func GetUserById(uid string) *User {
	var user User
	common.GetDBInstance().Where("id=?", uid).First(&user)
	return &user
}

func GetUserByUsername(username string) *User {
	var user User
	common.GetDBInstance().Where("username=?", username).First(&user)
	return &user
}

func (u *User) CreateOne() *User {
	common.GetDBInstance().Create(&u)
	return u
}

func (u *User) Update() {
	common.GetDBInstance().Save(&u)
}

func (u *User) CreatePassword(password string) error {
	pass, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.MinCost)
	u.Password = pass
	if err != nil {
		return err
	}
	return nil
}

func (u User) CheckPassword(password string) error {
	err := bcrypt.CompareHashAndPassword(u.Password, []byte(password))
	return err
}

func (u User) GenerateToken() (string, error) {
	token := jwt.New(jwt.GetSigningMethod("HS256"))
	token.Claims = jwt.MapClaims{
		"user":      string(u.Username),
		"is_owner":  u.IsStoreOwner,
		"userid":    u.ID,
		"is_member": u.IsMember,
		"exp":       time.Now().Add(time.Hour * 1).Unix(),
	}
	return token.SignedString([]byte("b1sm1llahirahmanirrahim1234567890987676765656"))
}

func (u User) Store() *Store {
	if u.IsStoreOwner && u.IsCashier {
		person := Persons{}
		common.GetDBInstance().Where("user_id=?", u.ID).Find(&person)
		store := Store{}

		common.GetDBInstance().Where("id=?", person.StoreID).Find(&store)
		return &store
	}
	return nil
}
