package models

import "github.com/jinzhu/gorm"

type Address struct {
	gorm.Model
	Address string
	Lat     float64
	Lng     float64
	Desc    string `gorm:"type:text"`
}
