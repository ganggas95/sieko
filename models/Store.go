package models

import "github.com/jinzhu/gorm"

type StoreCategory struct {
	gorm.Model
	Name string
}

type Store struct {
	StoreName  string `gorm:"type:varchar(45)"`
	KodeDesa   string `gorm:"type:varchar(10)"`
	AddressID  int64  `gorm:"type:bigint;index:address_id"`
	CategoryID int64  `gorm:"type:bigint;index:category_id"`
	ContactID  int64  `gorm:"type:bigint;index:contact_id"`
	ImageID    int64  `gorm:"type:bigint;index:image_id"`
	gorm.Model
}

type RatingStore struct {
	ChatRating     int64
	CancelRating   int64
	DeliveryRating int64
	StoreID        int64 `gorm:"type:bigint;index:store_id"`
	gorm.Model
}
