package models

import "github.com/jinzhu/gorm"

type Persons struct {
	gorm.Model
	FirstName   string `gorm:"type:varchar(46)"`
	LastName    string `gorm:"type:varchar(46)"`
	AddressID int64 `gorm:"type:bigint;index:address_id"`
	ContactID int64 `gorm:"type:bigint;index:contact_id"`
	UserID    int64 `gorm:"type:bigint;index:user_id"`
	ImageID   int64 `gorm:"type:bigint;index:image_id"`
	StoreID     uint64
}
