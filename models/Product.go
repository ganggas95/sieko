package models

import (
	"github.com/jinzhu/gorm"
	"sieko_core/common"
)

type Product struct {
	StoreID  int64   `gorm:"type:bigint;index:store_id"`
	Name     string  `gorm:"type:varchar(45)"`
	Price    float64 `gorm:"type:double"`
	Discount float32 `gorm:"type:float"`
	Trash    bool    `gorm:"type:tinyint"`
	CreateBy int64   `gorm:"type:bigint;index:create_by_id"`
	UpdateBy int64   `gorm:"type:bigint;index:update_by_id"`
	DeleteBy int64   `gorm:"type:bigint;index:delete_by_id"`
	gorm.Model
}

type ImagesProduct struct {
	gorm.Model
	ImageID   int64 `gorm:"type:bigint;index:image_id"`
	Primary   bool
	ProductID int64 `gorm:"type:bigint;index:product_id"`
}

type ProductRating struct {
	gorm.Model
	Quality int
}


func GetByID(idProduct uint) (*Product){
	product := Product{}
	common.GetDBInstance().Where("id=?", idProduct).Find(&product)
	return &product
}

func GetByStoreID(idStore int64) (*Product){
	product := Product{StoreID: idStore}
	common.GetDBInstance().Find(&product)
	return &product
}
