package models

import "github.com/jinzhu/gorm"

type Contact struct {
	gorm.Model
	MobilePhone string `gorm:"type:varchar(16)"`
	Email       string `gorm:"type:varchar(100)"`
}
