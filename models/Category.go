package models

import "github.com/jinzhu/gorm"

type Category struct {
	Name string `gorm:"type:varchar(45)"`
	Desc string `gorm:"type:text"`
	gorm.Model
}

type ProductCategory struct {
	IdProduct  int64 `gorm:"type:bigint;index:id_product"`
	IdCategory int64 `gorm:"type:bigint;index:id_category"`
	gorm.Model
}
