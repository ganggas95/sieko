package models

import (
	"sieko_core/common"
)

func Migrate() {
	db := common.GetDBInstance()
	db.AutoMigrate(Image{})
	db.AutoMigrate(Product{})
	db.AutoMigrate(ImagesProduct{})
	db.AutoMigrate(Category{})
	db.AutoMigrate(ProductCategory{})
	db.AutoMigrate(StoreCategory{})
	db.AutoMigrate(Store{})
	db.AutoMigrate(RatingStore{})

	db.AutoMigrate(UserStore{})
	db.AutoMigrate(User{})

	db.AutoMigrate(Contact{})
	db.AutoMigrate(Address{})

	db.AutoMigrate(ProductRating{})

	db.AutoMigrate(Persons{})
}
