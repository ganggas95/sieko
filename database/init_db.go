package database

import (
	// Add sql instance for mysql connection
	_ "database/sql"

	// Add sql instance for mysql connection
	_ "github.com/GoogleCloudPlatform/cloudsql-proxy/proxy/dialers/mysql"
	"github.com/jinzhu/gorm"
)

// DBEngine is variable to store database engine instance
var DBEngine *gorm.DB

// InitDatabase is function to initialize connection of database
func InitDatabase() {
	connString := "root:b1sm1llah@/sieko_dev_db?charset=utf8&parseTime=True&loc=UTC"
	db, err := gorm.Open("mysql", connString)

	if err != nil {
		println(err.(error).Error())
		panic("Error in connect database")
	}

	DBEngine = db

	println("Database connected")
}
