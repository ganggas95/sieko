package routers
import (
	"github.com/gin-gonic/gin"
	"github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
	"sieko_core/controllers"
	"sieko_core/middleware"
)

// @title Swagger Example API
// @version 1.0
// @description This is a sample server Petstore server.
// @termsOfService http://swagger.io/terms/

// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email support@swagger.io

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @host petstore.swagger.io
// @BasePath /api/v0.1
// InitRouters this function for initialize router in this service.
func InitRouters(router *gin.Engine) {
	//apiRouter : Initialization apiRouter as base URL
	apiRouter := router.Group("/api/v0.1")

	//apiRegisterRouter for register process
	apiRegisterRouter := apiRouter.Group("/register")
	apiRegisterRouter.POST("/store", controllers.RegisterStoreController)
	apiRegisterRouter.POST("/member", controllers.RegisterMemberController)
	apiRegisterRouter.POST("/confirm_phone", controllers.ConfirmPhoneNumberController)
	apiRegisterRouter.POST("/confirm_email", controllers.ConfirmEmailController)
	apiRegisterRouter.POST("/confirm_facebook", controllers.ConfirmFacebookController)

	//apiAuthRouter for register process
	apiAuthRouter := apiRouter.Group("/auth")
	apiAuthRouter.POST("/login", controllers.Auth)

	//apiRouterMember
	apiRouterMember := apiRouter.Group("/member")
	apiRouterMember.Use(middleware.MemberMiddleware)
	apiRouterMember.GET("/my_profile", controllers.MyProfile)

	//apiRouterUser
	apiRouterUser := apiRouter.Group("/user")
	apiRouterUser.Use(middleware.StoreMiddleware)
	apiRouterUser.POST("/new", controllers.NewUser)
	apiRouterUser.Any("/data/:user_id", controllers.GetUser)

	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
}
