package main

import (
	"github.com/gin-gonic/gin"
	"sieko_core/database"
	"sieko_core/models"
	"sieko_core/routers"
)

func main(){
	router := gin.Default()
	//gin.SetMode(gin.ReleaseMode)
	routers.InitRouters(router)
	database.InitDatabase()
	models.Migrate()
	router.Run(":8001")
}
