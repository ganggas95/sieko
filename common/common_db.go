package common

import (
	"github.com/jinzhu/gorm"
	"sieko_core/database"
)

//GetDBInstance Function to get database instance
func GetDBInstance() *gorm.DB {
	return database.DBEngine
}
