package common

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
)

//ParseJWTToken Function to parsing token. Params tokenString
func ParseJWTToken(tokenString string) (*jwt.Token, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte("b1sm1llahirahmanirrahim1234567890987676765656"), nil
	})

	return token, err
}
