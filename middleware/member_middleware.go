package middleware

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"sieko_core/common"
	"sieko_core/models"
	"net/http"
)

//MemberMiddleware Middleware to handle request guard of member
func MemberMiddleware(c *gin.Context) {
	token := c.GetHeader("Authorization")

	tokenString, err := common.ParseJWTToken(token)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"data": nil,
			"msg":  "Request not allowed!!",
		})
	} else {
		claims := tokenString.Claims.(jwt.MapClaims)
		user := models.GetUserById(claims["user"].(string))
		if user.ID != 0 && user.IsMember{
			c.Next()
		}
	}
}
