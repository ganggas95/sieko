package middleware

import (
	"net/http"
	"sieko_core/common"
	"sieko_core/models"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

//StoreMiddleware Middleware to check user Store
func StoreMiddleware(c *gin.Context) {
	token := c.GetHeader("Authorization")

	tokenString, err := common.ParseJWTToken(token)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"data": nil,
			"msg":  "Request not allowed!!",
		})
	} else {
		claims := tokenString.Claims.(jwt.MapClaims)
		user := models.GetUserById(claims["user"].(string))
		if user.ID != 0 && user.IsStoreOwner {
			c.Next()
		}
	}
}
