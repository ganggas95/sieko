FROM golang:alpine

ARG pkg=sieko_core

RUN apk add --no-cache ca-certificates

COPY . $GOPATH/src/$pkg

RUN set -ex \
      && apk add --no-cache --virtual .build-deps \
              git \
      && go get -v $pkg/... \
      && apk del .build-deps

RUN go install $pkg/...
#ENV PATH='$GOPATH/src/$pkg/app:'
WORKDIR $GOPATH/src/$pkg/app

# Users of the image should invoke either of the commands.

RUN go build -o $GOPATH/bin/siekocore ./
EXPOSE 8001
